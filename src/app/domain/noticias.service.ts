import {Injectable} from "@angular/core"
import { Http } from '@nativescript/core'
const sqlite = require("nativescript-sqlite")

@Injectable()
export class NoticiasService{

  api: string = "https://26da-41-79-50-150.ngrok.io";

  constructor(){
    this.getDb((db) => {
      console.dir(db);
      db.each("select * from logs",
        (err, fila) => console.log("fila: ", fila),
        (err, totales) => console.log("filas totales: ", totales));
    }, () => console.log("error on getDb"));
  }

  getDb(fnOk, fnError){
      return new sqlite("mi_db_logs", (err, db) => {
           if(err){
              console.error("error al abrir db!", err);
           }else{
              console.log("Esta la db abierta: ", db.isOpen()? "SI" : "NO");
              db.execSQL("CREATE TABLE IF NOT EXIST logs (id, INTEGER, PRIMARY kEY AUTOINCREMT, texto TEXT)"))
              .then((id) =>{
                  console.log("CREATE TABLE OK");
                  fnOk(db);
                }, (error) => {
                  console.log("CREATE TABLE ERROR", error);
                  fnError(error)
                });
           }
      });
  }

  agregar(s: string){
    return Http.request({
      url: this.api + "/favs",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
          nuevo: s
      })
    });
  }

  favs(){
     return Http.getJSON(this.api + "/favs");
  }

  buscar(s: string) {

     this.getDb((db) => {
          db.execSQL("insert into logs (texto) values (?)", [s], (err, id) => console.log("nuevo id: ", id));
      },() => console.log("error on getDB"));

    return Http.getJSON(this.api + "/get?q=" + s);
  }

}
