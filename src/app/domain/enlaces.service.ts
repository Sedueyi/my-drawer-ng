import {Injectable} from "@angular/core"

@Injectable()
export class EnlacesService{

   public enlaces: Array<string> = []

    agregar(e: string){
      this.enlaces.push(e)
    }

   buscar(){
      return this.enlaces
   }
}
