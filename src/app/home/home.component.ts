import { Component, OnInit } from '@angular/core'
import { RadSideDrawer } from 'nativescript-ui-sidedrawer'
import { Application } from '@nativescript/core'
import { EnlacesService } from '../domain/enlaces.service'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule } from '@nativescript/angular'
import { NavigationEnd, Router } from '@angular/router'
import { RouterExtensions } from '@nativescript/angular'
import {
  DrawerTransitionBase,
  SlideInOnTopTransition,
} from 'nativescript-ui-sidedrawer'
import { filter } from 'rxjs/operators'


@Component({
  selector: 'Home',
  moduleId: module.id,
  templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {

  private _activatedUrl: string
  private _sideDrawerTransition: DrawerTransitionBase

  constructor(public enlaces:EnlacesService,private router: Router, private routerExtensions: RouterExtensions) {
    // Use the component constructor to inject providers.

  }

  ngOnInit(): void {
    //Init your component properties here.


           this.enlaces.agregar("Browse")
           this.enlaces.agregar("search")
           this.enlaces.agregar("Featured")
           this.enlaces.agregar("Contact")
           this.enlaces.agregar("Settings")


             this._activatedUrl = '/'
                 this._sideDrawerTransition = new SlideInOnTopTransition()

                 this.router.events
                 .pipe(filter((event: any) => event instanceof NavigationEnd))
                 .subscribe((event: NavigationEnd) => (this._activatedUrl = event.urlAfterRedirects))
  }


      isComponentSelected(url: string): boolean {
        return this._activatedUrl === url
      }

       onDrawerButtonTap(): void {
                const sideDrawer = <RadSideDrawer>Application.getRootView()
                sideDrawer.showDrawer()
        }


     onNavItemTap(navItemRoute: string): void {
       this.routerExtensions.navigate([navItemRoute], {
         transition: {
           name: 'fade',
         },

        })
     }

}
