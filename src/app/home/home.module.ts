import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'
import { HomeRoutingModule } from './home-routing.module'
import { HomeComponent } from './home.component'
import { EnlacesService } from '../domain/enlaces.service'

@NgModule({
  imports: [NativeScriptCommonModule, HomeRoutingModule],
  declarations: [HomeComponent],
  providers:[EnlacesService],
  schemas: [NO_ERRORS_SCHEMA],
})
export class HomeModule {}
