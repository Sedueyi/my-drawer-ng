import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule } from '@nativescript/angular'
import { ContactComponent } from './contact.component'

const routes: Routes =[
{ path: '', component: ContactComponent },
  {
        path: 'profile',
        loadChildren: () => import('~/app/profile/profile.module').then((m) => m.ProfileModule),
   },
]

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule],
})
export class ContactRoutingModule {}
