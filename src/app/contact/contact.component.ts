import { Component, OnInit } from '@angular/core'
import { RadSideDrawer } from 'nativescript-ui-sidedrawer'
import { Application } from '@nativescript/core'
import { NavigationEnd, Router } from '@angular/router'
import { RouterExtensions } from '@nativescript/angular'
import {
  DrawerTransitionBase,
  SlideInOnTopTransition,
} from 'nativescript-ui-sidedrawer'
import { filter } from 'rxjs/operators'


@Component({
  selector: 'Contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {

    private _activatedUrl: string
    private _sideDrawerTransition: DrawerTransitionBase

  constructor(private router: Router, private routerExtensions: RouterExtensions) {
      // Use the component constructor to inject services.
    }

  ngOnInit(): void {

    // Init your component properties here.
      this._activatedUrl = '/'
      this._sideDrawerTransition = new SlideInOnTopTransition()

      this.router.events
      .pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => (this._activatedUrl = event.urlAfterRedirects))


   }

    isComponentSelected(url: string): boolean {
       return this._activatedUrl === url
     }

      onDrawerButtonTap(): void {
               const sideDrawer = <RadSideDrawer>Application.getRootView()
               sideDrawer.showDrawer()
       }



    onNavItemTap(navItemRoute: string): void {
      this.routerExtensions.navigate([navItemRoute], {
        transition: {
          name: 'fade',
        },

       })


  }




}
