import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'
import { ContactRoutingModule } from './contact-routing.module'
import { ContactComponent } from './contact.component'
import { ProfileRoutingModule } from '../profile/profile-routing.module'
import { ProfileComponent } from '../profile/profile.component'


@NgModule({
  imports: [
      NativeScriptCommonModule,
      ContactRoutingModule,
      ProfileRoutingModule,
  ],
  declarations: [ContactComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class ContactModule {}
