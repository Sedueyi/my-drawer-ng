import {Component, EventEmitter, Output, Input, OnInit}  from '@angular/core'

@Component({
  selector:'SearchForm',
  moduleId: module.id,
  template: `
           <TextField [(ngModule)] = "textFieldValue" hint="Ingresar texto..."></TextField>
           <Button text="Buscar" (tap)="onButtonTap()"></Button>

            `
})

export class SearchFormComponent implements OnInit {
    textFieldValue:string = "";
    @Output() search: EventEmitter<String> = new EventEmitter();
    @Input() inicial: string;

     ngOnInit(): void {
        // Init your component properties here.
        this.textFieldValue = this.inicial;
      }

    onButtonTap(): void {
      console.log(this.textFieldValue);
        if(this.textFieldValue.length > 2){
          this.search.emit(this.textFieldValue);
        }
    }

}
