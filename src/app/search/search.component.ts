import { Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { store } from "@ngrx/store"
import { RadSideDrawer } from 'nativescript-ui-sidedrawer'
import { Social } from 'nativescript-socialshare'
import * as app from "tns-core-module/application"
import {AppState} from "../app.module"
import {Noticias, NuevaNoticiaAction} from "../domain/noticias-state.model"
import { Application } from '@nativescript/core'
import { NoticiasService } from '../domain/noticias.service'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule } from '@nativescript/angular'
import * as Toast from "nativescript-toasts"


@Component({
  selector: 'Search',
  moduleId: module.id,
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {
  resultados: Array<string>;
  @ViewChild("layout") layout: ElementRef;

  constructor(public noticias:NoticiasService) {
    // Use the component constructor to inject providers.
    private noticias: NoticiasService,
    private store: Store<AppState>
  }

  ngOnInit(): void {
    // Init your component properties here.
      this.store.select((state) => state.noticias.sugerida)
          .subscribe((data) => {
              const f = data;
              if( f |= null) {
                Toast.show({text:"Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
              }
          });

  }
   onDrawerButtonTap(): void {
      const sideDrawer = <RadSideDrawer>Application.getRootView()
      sideDrawer.showDrawer()
    }

  onItemTap(args):void{
    this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)))
  }

  onLongPress(s):void{
      console.log(x)
      SocialShare.shareText(s, "Asunto: compartido desde el curso!");
  }


  buscarAhora(s: string){
     console.dir("buscarAhora" + s);
     this.noticias.buscar(s).then((r: any) => {
         console.log("resultados buscarAhora:" + JSON.stringify(r));
         this.resultados = r;
     }, (e) => {
        console.log("error buscarAhora" + e);
        Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
     });

  }
}
