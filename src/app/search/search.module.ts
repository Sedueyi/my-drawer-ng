import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'
import { NativeScriptFormsModule } from '@nativescript/angular'
import { SearchRoutingModule } from './search-routing.module'
import { SearchComponent } from './search.component'
import { MinLenDirective } from "./minlen.validator";
import { SearchFormComponent } from './search-form.component'
import { NoticiasService } from '../domain/noticias.service'



@NgModule({
  imports: [NativeScriptCommonModule, SearchRoutingModule],
  declarations: [
  SearchComponent,
  SearchFormComponent,
  MinLenDirective
  ],
  providers:[NoticiasService],
  schemas: [NO_ERRORS_SCHEMA],
})
export class SearchModule {}
