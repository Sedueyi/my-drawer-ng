import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptModule } from '@nativescript/angular'
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'

import {EffectsModule} from "@ngrx/effects"
import {ActionIndexerMap, StoreModule as NgRxStoreModule} from "@ngrx/store"
import {
      InitializeNoticiasState,
      NoticiasEffects,
      NoticiasStat,
      ReducersNoticias
 } from "./domian/noticias-state.model"
 import {NoticiasService} from "./domain/noticias.service"

export Interface AppState{
  noticas: NoticiasState;
}
const reducers: ActionReducerMap<AppState> = {
    noticias: reducerNoticias
}

const reducersInitialState = {
   noticias: InitializeNoticiasState()
}
@NgModule({
  bootstrap: [AppComponent],
  imports: [
    AppRoutingModule,
    NativeScriptModule,
     NativeScriptUISideDrawerModule,
     NgRxStoreModule.forRoot(reducers,{intialState: reducersInitialState}),
     EffectsModule.forRoot({NoticiasEffects})

  ],
  providers: [NoticiasService],
  declarations: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
